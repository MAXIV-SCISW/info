/*global $*/
'use strict';

// The gloabl variables for this applicaiton
var MAIN_APP = {

    buttonConfig : [

        //JupyterHub
        {   name    : 'JupyterHub Repositories',
            color   : 'white',
            page    : 'https://gitlab.com/MAXIV-SCISW/JUPYTERHUB',
            div     : 'divMAXIVSciSWJupyterHub',
            icon    : 'glyphicon-cloud-download',
            xsShow  : true,
            group   : 'jhub'},

        {   name    : 'Jupyter Docker Image Registry',
            color   : 'white',
            page    : 'https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/' +
                'jupyter-docker-stacks/container_registry',
            div     : 'divMAXIVSciSWJupyterDockerImageRegistry',
            icon    : 'glyphicon-list',
            xsShow  : true,
            group   : 'jhub'},

        {   name    : 'Jupyter Validation Presentation 2021-04',
            color   : 'white',
            page    : 'presentations/2021-04-29-Jupyter-Kernel-Validation/',
            div     : 'divJHubValidation20210120',
            icon    : 'glyphicon-ok',
            xsShow  : true,
            group   : 'jhub'},

        // HDF5
        {   name    : 'HDF5 Viewer Repositories',
            color   : 'white',
            page    : 'https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER',
            div     : 'divMAXIVSciSWHDF5Viewer',
            icon    : 'glyphicon-cloud-download',
            xsShow  : true,
            group   : 'hdf5'},

        {   name    : 'HDF5 Viewer Docker Image Registry',
            color   : 'white',
            page    : 'https://gitlab.com/groups/MAXIV-SCISW/HDF5-VIEWER/' +
                '-/container_registries',
            div     : 'divMAXIVSciSWHDF5DockerImageRegistry',
            icon    : 'glyphicon-list',
            xsShow  : true,
            group   : 'hdf5'},

        {   name    : 'HDF5 Viewer Presentation 2019-09',
            color   : 'white',
            page    : 'presentations/2019-09-18-HDF5-Workshop/',
            div     : 'divHDF5ViewerPresentation20190918',
            icon    : 'glyphicon-ok',
            xsShow  : true,
            group   : 'hdf5'},

    ]
};
