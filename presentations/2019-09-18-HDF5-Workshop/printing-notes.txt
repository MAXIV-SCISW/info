[2019-09-12 - Printing Notes]

	* To save a presentation to pdf, add ?print-pdf to the end of the
	  presentation url, like so:
		https://jasbru.maxiv.lu.se:9443/jasons-presentations/presentations/
			2019-09-18-HDF5-Workshop/?print-pdf

	* To also print out the notes on separate pages:

		1) Edit the reveal.js file:
			vim /var/www/jasons-presentations/reveal-js/js/reveal.js
				showNotes: "separate-page"

		2) Reload the presentation, with ?print-pdf added to the url:
			https://jasbru.maxiv.lu.se:9443/jasons-presentations/presentations/
				2019-09-18-HDF5-Workshop/?print-pdf

		3) Undo the changes to the reveal.js file:
			vim /var/www/jasons-presentations/reveal-js/js/reveal.js
				showNotes: false,
